package com.stbu;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MainTest {
    @Test
    public void sqlTest() {
        for (int i = 8; i < 200; i++) {
            try {
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_anli", "root", "2003926");
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO building (bname, bunit, bfloor, bnumber, barea) VALUES (?,?,?,?,?)");
                preparedStatement.setString(1, Integer.toString(i * 11));
                preparedStatement.setString(2, Integer.toString(i * 11));
                preparedStatement.setInt(3, i * 11);
                preparedStatement.setInt(4, i * 11);
                preparedStatement.setFloat(5, (float)(i  + 0.5));
                preparedStatement.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Test
    public void employmentTest(){
        for (int i = 8; i < 200; i++) {
            try {
                Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/db_anli", "root", "2003926");
                PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO employment(ename, idcard, eaddr, enumber, eidentity) VALUES (?,?,?,?,?)");
                preparedStatement.setString(1, Integer.toString(i * 11));
                preparedStatement.setString(2, Integer.toString(i * 11));
                preparedStatement.setString(3, String.valueOf(i * 11));
                preparedStatement.setInt(4, i * 11);
                preparedStatement.setBoolean(5,true);
                preparedStatement.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
