create database db_anli;

use db_anli;

create table building
(
    bid     int primary key auto_increment,
    bname   varchar(50),
    bunit   varchar(50),
    bfloor  int,
    bnumber int,
    barea   float(10, 2)
);

create table employment
(
    eid       int primary key auto_increment,
    ename     varchar(50),
    idcard    varchar(50),
    eaddr     varchar(50),
    enumber   int,
    eidentity boolean
);

create table s_menu
(
    mid    int primary key auto_increment,
    mname  varchar(50),
    mlevel varchar(50),
    mtype  varchar(50)
);

create table s_user
(
    uid        int primary key auto_increment,
    uname      varchar(50),
    uloginname varchar(50),
    upassword  varchar(50),
    utype      varchar(50)
);

insert into s_user(uname, uloginname, upassword, utype)
VALUES ('root', 'root', '12345', 1)