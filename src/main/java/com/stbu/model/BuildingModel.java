package com.stbu.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data

public class BuildingModel extends QueryPage {
    Integer bid;
    String bname;
    String bunit;
    Integer bfloor;
    Integer bnumber;
    Float barea;
}
