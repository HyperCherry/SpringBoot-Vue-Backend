package com.stbu.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class EmploymentModel extends QueryPage {
    private Integer eid;
    private String ename;
    private String idcard;
    private String eaddr;
    private Integer enumber;
    private String eidentity;
}