package com.stbu.model;

import lombok.Data;

@Data
public class QueryPage {
    private Integer current;
    private Integer size;

    public Integer getCurrent() {
        return current != null ? current : 1;// 未传递参数时给予默认值，避免NPE
    }

    public Integer getSize() {
        return size != null ? size : 10;
    }
}
