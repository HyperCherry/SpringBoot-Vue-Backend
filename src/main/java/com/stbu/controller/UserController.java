package com.stbu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.stbu.entities.UserBean;
import com.stbu.service.UserService;
import com.stbu.util.RestResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@CrossOrigin("*")
public class UserController {
    public final UserService userService;

    @RequestMapping("/getAdminById")
    public RestResponse<UserBean> getAdminById() {
        UserBean userBean = userService.getById(1);
        if (userBean == null) {
            return RestResponse.fail("没有数据");
        } else {
            return RestResponse.ok(userBean);
        }
    }

    @RequestMapping("/getAdminByName")
    public RestResponse<UserBean> getAdminByName(String username, String password) {
        QueryWrapper<UserBean> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("uname", username);
        queryWrapper.eq("upassword", password);
        UserBean userBean = userService.getOne(queryWrapper);
        if (userBean == null) return RestResponse.fail("用户名或密码错误");
        if (Integer.parseInt(userBean.getUtype()) != 1) return RestResponse.fail("账号权限不足");
        return RestResponse.ok(userBean);
    }

    @RequestMapping("/updatePassword")
    public RestResponse<Integer> updateUser(int uid, String password) {
        try {
            UserBean userBean = new UserBean();
            userBean.setUpassword(password);
            userBean.setUid(uid);
            userService.updateById(userBean);
            return RestResponse.ok(uid);
        } catch (Exception e) {
            return RestResponse.fail("修改失败");
        }
    }

    @RequestMapping("/getAllUserName")
    public RestResponse<List<Map<String, Object>>> getAllUserName() {
        QueryWrapper<UserBean> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("uid", "uname");
        return RestResponse.ok(userService.getBaseMapper().selectMaps(queryWrapper));
    }

    @RequestMapping("/updateAdmin")
    public RestResponse<Integer> updateAdmin(int oldId, int newId) {
        try {
            UserBean oldUser = new UserBean();
            oldUser.setUtype(Integer.toString(2));
            QueryWrapper<UserBean> oldWrapper = new QueryWrapper<>();
            oldWrapper.eq("uid", oldId);
            userService.update(oldUser, oldWrapper);
            UserBean newUser = new UserBean();
            newUser.setUtype(Integer.toString(1));
            QueryWrapper<UserBean> newWrapper = new QueryWrapper<>();
            newWrapper.eq("uid", newId);
            userService.update(newUser, newWrapper);
            return RestResponse.ok(newId);
        } catch (Exception e) {
            return RestResponse.fail("无法更改管理员");
        }
    }
}