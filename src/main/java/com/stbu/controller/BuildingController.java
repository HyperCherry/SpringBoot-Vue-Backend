package com.stbu.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.stbu.entities.BuildingBean;
import com.stbu.model.BuildingModel;
import com.stbu.service.BuildingService;
import com.stbu.util.RestResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/building")
@RequiredArgsConstructor
@CrossOrigin("*")
public class BuildingController {
    private final BuildingService buildingService;
    @RequestMapping("/getLists")
    public RestResponse<Page<BuildingBean>> getAllBuildings(BuildingModel buildingModel){
        try{
            return RestResponse.ok(buildingService.getLists(buildingModel));
        }catch (Exception e){
            return RestResponse.fail("参数不合法");
        }
    }

    @RequestMapping("/insert")
    public RestResponse<BuildingBean> insertBuilding(BuildingBean buildingBean){
        try {
            if (buildingService.save(buildingBean)) {
                return RestResponse.ok(buildingBean);
            } else {
                return RestResponse.fail("数据插入失败");
            }
        }catch (Exception e){
            return RestResponse.fail("参数不合法");
        }
    }

    @RequestMapping("/update")
    public RestResponse<BuildingBean> updateEmployments(BuildingBean buildingBean) {
        try {
            if (buildingService.updateById(buildingBean)) {
                return RestResponse.ok(buildingBean);
            } else {
                return RestResponse.fail("数据更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail("参数不合法");
        }
    }

    @RequestMapping("/delete")
    public RestResponse<Integer> deleteEmploymentById(Integer eid) {
        try {
            if (buildingService.removeById(eid)) {
                return RestResponse.ok(eid);
            } else {
                return RestResponse.fail("数据删除失败");
            }
        } catch (Exception e) {
            return RestResponse.fail("参数不合法");
        }
    }
}
