package com.stbu.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.stbu.entities.EmploymentBean;
import com.stbu.model.EmploymentModel;
import com.stbu.service.EmploymentService;
import com.stbu.util.RestResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employment")
@RequiredArgsConstructor
@CrossOrigin("*")
public class EmploymentController {
    public final EmploymentService employmentService;

    @RequestMapping("/getLists")
    public RestResponse<Page<EmploymentBean>> getAllEmployments(EmploymentModel employmentModel) {
        try {
            return RestResponse.ok(employmentService.getLists(employmentModel));
        } catch (Exception e) {
            return RestResponse.fail("参数不合法");
        }
    }

    @RequestMapping("/insert")
    public RestResponse<EmploymentBean> insertEmployments(EmploymentBean employmentBean) {
        try {
            if (employmentService.getBaseMapper().insert(employmentBean) != 0) {
                return RestResponse.ok(employmentBean);
            } else {
                return RestResponse.fail("数据插入失败");
            }
        } catch (Exception e) {
            return RestResponse.fail("参数不合法");
        }
    }

    @RequestMapping("/update")
    public RestResponse<EmploymentBean> updateEmployments(EmploymentBean employmentBean) {
        try {
            if (employmentService.updateById(employmentBean)) {
                return RestResponse.ok(employmentBean);
            } else {
                return RestResponse.fail("数据更新失败");
            }
        } catch (Exception e) {
            return RestResponse.fail("参数不合法");
        }
    }

    @RequestMapping("/delete")
    public RestResponse<Integer> deleteEmploymentById(Integer eid) {
        try {
            if (employmentService.removeById(eid)) {
                return RestResponse.ok(eid);
            } else {
                return RestResponse.fail("数据删除失败");
            }
        } catch (Exception e) {
            return RestResponse.fail("参数不合法");
        }
    }
}
