package com.stbu.entities;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("building")
public class BuildingBean {
    @TableId(type = IdType.INPUT)
    Integer bid;
    String bname;
    String bunit;
    Integer bfloor;
    Integer bnumber;
    Float barea;
}
