package com.stbu.entities;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("s_menu")
public class MenuBean {
    @TableId(type = IdType.INPUT)
    Integer mid;
    String mname;
    String mlevel;
    String mtype;
}
