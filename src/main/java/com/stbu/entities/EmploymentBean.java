package com.stbu.entities;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("employment")
public class EmploymentBean {
    @TableId(type = IdType.INPUT)
    private Integer eid;
    private String ename;
    private String idcard;
    private String eaddr;
    private Integer enumber;
    private Boolean eidentity;
}
