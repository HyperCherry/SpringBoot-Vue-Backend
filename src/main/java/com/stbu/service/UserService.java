package com.stbu.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stbu.entities.UserBean;
import com.stbu.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService extends ServiceImpl<UserMapper, UserBean> {

}
