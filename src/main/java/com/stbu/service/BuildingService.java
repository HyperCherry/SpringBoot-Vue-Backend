package com.stbu.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stbu.entities.BuildingBean;
import com.stbu.mapper.BuildingMapper;
import com.stbu.model.BuildingModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BuildingService extends ServiceImpl<BuildingMapper, BuildingBean> {
    public Page<BuildingBean> getLists(BuildingModel buildingModel) {
        Integer size = buildingModel.getSize();
        Integer current = buildingModel.getCurrent();
        BuildingBean buildingBean = new BuildingBean();
        QueryWrapper<BuildingBean> queryWrapper = new QueryWrapper<>();
        queryWrapper.setEntity(buildingBean);
        return baseMapper.selectPage(Page.of(current, size), queryWrapper);
    }
}
