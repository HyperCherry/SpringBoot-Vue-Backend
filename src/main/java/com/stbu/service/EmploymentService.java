package com.stbu.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.stbu.entities.EmploymentBean;
import com.stbu.mapper.EmploymentMapper;
import com.stbu.model.EmploymentModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmploymentService extends ServiceImpl<EmploymentMapper, EmploymentBean> {

    public Page<EmploymentBean> getLists(EmploymentModel employmentModel) {
        Integer current = employmentModel.getCurrent();
        Integer size = employmentModel.getSize();
        EmploymentBean emp = new EmploymentBean();
        QueryWrapper<EmploymentBean> wrapper = new QueryWrapper<>();
        wrapper.setEntity(emp);
        return baseMapper.selectPage(Page.of(current, size), wrapper);
    }
}
