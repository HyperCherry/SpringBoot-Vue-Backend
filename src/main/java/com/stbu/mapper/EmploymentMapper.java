package com.stbu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stbu.entities.EmploymentBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EmploymentMapper extends BaseMapper<EmploymentBean> {
}
