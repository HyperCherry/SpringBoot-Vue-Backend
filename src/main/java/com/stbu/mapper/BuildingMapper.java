package com.stbu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.stbu.entities.BuildingBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BuildingMapper extends BaseMapper<BuildingBean> {
}
